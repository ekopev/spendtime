require 'compass-normalize'
require 'ninesixty'
require 'canvas'

sass_dir        = ''
css_dir         = '../styles'
images_dir      = '../images'
# output_style    = :nested
relative_assets = true
line_comments   = false


#output_style  = :compressed
#environment   = :production

output_style  = :expanded
environment   = :development